import time
import os
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models import create_model
from util.visualizer import Visualizer
from pdb import set_trace as st
from util import html
from util.metrics import PSNR
from util.metrics import SSIM
from PIL import Image

if __name__ == '__main__':
    opt = TestOptions().parse()
    opt.nThreads = 1
    opt.batchSize = 1
    opt.serial_batches = True
    opt.no_flip = True
    opt.phase = 'test'

    data_loader = CreateDataLoader(opt)
    dataset = data_loader.load_data()
    model = create_model(opt)
    visualizer = Visualizer(opt)

    web_dir = os.path.join(opt.results_dir, opt.name,
                           '%s_%s' % (opt.phase, opt.which_epoch))

    webpage = html.HTML(
        web_dir, 'Experiment = %s, Phase = %s, Epoch = %s' % (opt.name, opt.phase,
                                                              opt.which_epoch))
    # test

    avgPSNR = 0.0
    avgPSNR_b = 0.0
    avgSSIM = 0.0
    avgSSIM_b = 0.0
    counter = 0

    print(dataset.dataset.__len__())

    for i, data in enumerate(dataset):
        if i >= opt.how_many:
            break
        counter = i
        print(i)

        model.set_input(data)
        model.test()
        visuals = model.get_current_visuals()
        visualizer.display_current_results(visuals, counter)
        print(visuals)
        img_path = model.get_image_paths()
        visualizer.save_images(webpage, visuals, img_path)
